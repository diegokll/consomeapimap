package br.com.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
@JsonPropertyOrder({ "latitude", "longitude" })
@JsonIgnoreProperties(value = {"ip","type","continent_code","continent_name","country_code","country_name",
		"region_code","region_name","city","zip","location","time_zone","currency","connection","security"})
public class LocalGeoLocation {
	
	@JsonProperty("latitude")
	String latitide;
	
	@JsonProperty("longitude")
	String longitude;

	public String getLatitide() {
		return latitide;
	}

	public void setLatitide(String latitide) {
		this.latitide = latitide;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
}
