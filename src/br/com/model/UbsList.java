package br.com.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value = { "pageable","totalPages","totalElements","last","first","sort","numberOfElements","size","number","empty"})
public class UbsList {
	
	@JsonProperty("content")
	List<Ubs> ubsList;

	public List<Ubs> getUbsList() {
		return ubsList;
	}

	public void setUbsList(List<Ubs> ubsList) {
		this.ubsList = ubsList;
	}
	
}
