package br.com.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GeoCode {
	
	@JsonProperty("lati")
	String lati;
	
	@JsonProperty("longi")
	String longi;

	public String getLati() {
		return lati;
	}

	public void setLati(String lati) {
		this.lati = lati;
	}

	public String getLongi() {
		return longi;
	}

	public void setLongi(String longi) {
		this.longi = longi;
	}
	
	
}
