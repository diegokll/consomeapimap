package br.com.controler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import br.com.model.LocalGeoLocation;
import br.com.model.Ubs;
import br.com.model.UbsList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ManagedBean(name="controller")
@ViewScoped
public class testeController implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String zoom="5";
	
	private String lati;
	
	private String longi;
	
	private String latiLocal;
	
	private String longiLocal;
	
	private MapModel emptyModel;
	
	private String center="-12.409626, -50.291101";
	
	private String URL_IP_API;
	
	private String URL_UBS_API;
	
	private String teste; //Variavel SEMPRE com gets e sets ou n funciona nada

	private List<Ubs> listaUbs = new ArrayList<Ubs>();
	
	//Inicia o mapa sem marcadores caso n�o encontre a localiza��o baseada no IP
	//Inicia o mapa com os marcadores ao redor do local atual baseado no IP
	@PostConstruct
	public void init() throws IOException{
		
		emptyModel = new DefaultMapModel();
		
		String ip = pegarIp();
		
		URL_IP_API = "http://api.ipstack.com/"+ip+"?access_key=b6b6de6d587a0190d9e1c1c790aa6c16&format=1";
		
		localAtual();
		

	}
	
	//metodo para consultar API e preencher mapa com marcadores
	public void consultaAPI() throws JsonMappingException, JsonProcessingException, UnknownHostException{
				
		//seta os parametros iniciais 
		setarParametros();
		
		//consulta API e tr�s o retorno Json para lista
		ObjectMapper objectMapper = new ObjectMapper();		
		UbsList ubs = objectMapper.readValue(getJson(URL_UBS_API),UbsList.class);
		
		//cria os marcadores no mapa
		criarMarcadores(ubs);
				
	}
	
	public void consultaAPILocal() throws JsonMappingException, JsonProcessingException, UnknownHostException{
		
		//seta os parametros iniciais 
		setarParametrosLocal();
		
		//consulta API e tr�s o retorno Json para lista
		ObjectMapper objectMapper = new ObjectMapper();		
		UbsList ubs = objectMapper.readValue(getJson(URL_UBS_API),UbsList.class);
		
		//cria os marcadores no mapa
		criarMarcadores(ubs);
				
	}
	
	public void setarParametros() throws UnknownHostException, JsonMappingException, JsonProcessingException{
		
		setZoom("12");
		
		emptyModel = new DefaultMapModel();
		
		Marker marker = new Marker(new LatLng( Double.parseDouble(lati), Double.parseDouble(longi)), "In�cio");
        
		marker.setIcon("http://maps.google.com/mapfiles/ms/micons/blue.png");
		
		emptyModel.addOverlay(marker);
		
		center	= lati+","+longi;	
		
		URL_UBS_API = "https://case-bionexo.herokuapp.com/api/v1/find_ubs?"
				+ "query="+lati+","+longi+"&page=0&per_page=10";
				
	}
	
	public void setarParametrosLocal() throws UnknownHostException, JsonMappingException, JsonProcessingException{
		
		setZoom("15");
		
		center=latiLocal+","+longiLocal;

		emptyModel = new DefaultMapModel();
		
		Marker marker = new Marker(new LatLng( Double.parseDouble(latiLocal), Double.parseDouble(longiLocal)), "Local Atual Baseado em IP");
        
		marker.setIcon("http://maps.google.com/mapfiles/ms/micons/blue.png");
		
		emptyModel.addOverlay(marker);
		
		URL_UBS_API = "https://case-bionexo.herokuapp.com/api/v1/find_ubs?"
				+ "query="+latiLocal+","+longiLocal+"&page=0&per_page=10";
		
	}
	
	public String getJson(String URL){
		
		Client client = ClientBuilder.newClient();
		
		WebTarget target = client.target(URL);
		
		Response response = target.request().get();

		String json = response.readEntity(String.class);
				
		response.close();
		
		return json;
	}
	
	public void criarMarcadores(UbsList ubs){
		
		//varre lista e cria marcadores para o mapa adicionando tamb�m informa��es aos marcadores
		for (Ubs item : ubs.getUbsList()) {
			addMarker("Nome: "+item.getName()+"\n"
					+"Endere�o: "+item.getAddress()+"\n"
					+"Telefone: "+item.getPhone(), 
					Double.parseDouble(item.getGeoCode().getLati()), 
					Double.parseDouble(item.getGeoCode().getLongi()));
		}
	}
	
		//adiciona marcador ao mapa
		public void addMarker(String title,double lati, double longi) {
	        Marker marker = new Marker(new LatLng(lati, longi), title);
	        emptyModel.addOverlay(marker);
	    }

	public String getIpLocal() throws UnknownHostException{
			
		String ip=InetAddress.getLocalHost().getHostAddress();
		System.out.println(ip);
		    
		   return ip.toString();
		}
		
	public void localAtual() throws JsonMappingException, JsonProcessingException, UnknownHostException{
		
		ObjectMapper objectMapper = new ObjectMapper();
			
		LocalGeoLocation local = objectMapper.readValue(getJson(URL_IP_API),LocalGeoLocation.class);
		
		//se for possivel definir a localiza��o atual pelo IP o centro do mapa ser� esse local
		//e uma busca ser� disparada a partir deste local
		if(local.getLatitide()!=null && local.getLongitude()!=null){
			latiLocal=local.getLatitide();
			longiLocal=local.getLongitude();
			consultaAPILocal();
		}else{
			System.out.println("N�o foi poss�vel definir o local atual.");
		}
		
	}
	
	public String pegarIp() throws IOException{
		
		String addr = "http://meuip.com/api/meuip.php";
		URL url = new URL(addr);
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		httpCon.setRequestMethod("GET");

		 BufferedReader in = new BufferedReader(
                 new InputStreamReader(httpCon.getInputStream()));
         String inputLine;
         StringBuffer response = new StringBuffer();
   
         while ((inputLine = in.readLine()) != null) {
             response.append(inputLine);
         }
         in.close();
   
         //print result
         System.out.println("seu ip �:" + response.toString());
		
		return response.toString();
		
	}
			
		
	public MapModel getEmptyModel() {
		return emptyModel;
	}

	public void setEmptyModel(MapModel emptyModel) {
		this.emptyModel = emptyModel;
	}

	public String getTeste() {
		return teste;
	}

	public void setTeste(String teste) {
		this.teste = teste;
	}

	public String getURL_UBS_API() {
		return URL_UBS_API;
	}

	public void setURL_UBS_API(String uRL_UBS_API) {
		URL_UBS_API = uRL_UBS_API;
	}

	public List<Ubs> getListaUbs() {
		return listaUbs;
	}

	public void setListaUbs(List<Ubs> listaUbs) {
		this.listaUbs = listaUbs;
	}


	public String getCenter() {
		return center;
	}


	public void setCenter(String center) {
		this.center = center;
	}


	public String getLati() {
		return lati;
	}


	public void setLati(String lati) {
		this.lati = lati;
	}


	public String getLongi() {
		return longi;
	}


	public void setLongi(String longi) {
		this.longi = longi;
	}


	public String getZoom() {
		return zoom;
	}


	public void setZoom(String zoom) {
		this.zoom = zoom;
	}

	public String getURL_IP_API() {
		return URL_IP_API;
	}

	public void setURL_IP_API(String uRL_IP_API) {
		URL_IP_API = uRL_IP_API;
	}

	public String getLatiLocal() {
		return latiLocal;
	}

	public void setLatiLocal(String latiLocal) {
		this.latiLocal = latiLocal;
	}

	public String getLongiLocal() {
		return longiLocal;
	}

	public void setLongiLocal(String longiLocal) {
		this.longiLocal = longiLocal;
	}
	
	
	
}